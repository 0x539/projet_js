<?php
session_start();

$_SESSION = array();
if (ini_get("session.use_cookies")) {
  $paramètres = session_get_cookie_params ();
  setcookie (session_name (), '', time () - 42000,
             $paramètres['path'], $paramètres['domain'],
             $paramètres['secure'], $paramètres['httponly']);
}

session_destroy ();
