<?php
session_start();

$résultat = new stdClass();
$résultat->résultat = true;
$résultat->message = '';
$résultat->est_connecté = false;

if (isset($_SESSION['id_user'])) {
  $résultat->est_connecté = true;
}

header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');
echo json_encode($résultat);
