(use-modules (web server)
             (web http)
             (web request)
             (web response)
             (web uri)
             (json)
             (ice-9 rdelim)
             (sxml simple)
             (rnrs bytevectors))

(setlocale LC_ALL "")

(define racine (getcwd))

(define texte "αβγδεζηθικλμνξοπρστυφχψω\nΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΤΥΦΧΨΩ")

(define utilisateurs '(("utilisateur1" . "mdp1")
                       ("utilisateur2" . "mdp2")
                       ("utilisateur3" . "mdp3")))

(define sessions '())

(define* (créer-identifiant-session #:optional (tentatives 0))
  (if (not (>= tentatives 10))
      (let ((nombre (random 1000000000 (random-state-from-platform))))
        (if (member nombre sessions)
            (créer-identifiant-session (+ tentatives 1))
            nombre))))

(define (session-correcte? session)
  (if (member session sessions) #t #f))

(define (supprimer-session session)
  (delq! session sessions))

(define (identifiants-corrects? utilisateur mot-de-passe)
  (let ((identifiant `(,utilisateur . ,mot-de-passe)))
    (if (member identifiant utilisateurs)
        #t #f)))

(define (composants-chemin-requête requête)
  (split-and-decode-uri-path (uri-path (request-uri requête))))

(define (non-trouvé requête)
  (values (build-response #:code 404)
          (string-append "Ressource non trouvé : « "
                         (uri->string (request-uri requête))
                         " ».")))

(define* (lire-tout-le-fichier port #:optional (chaîne ""))
  (let* ((ligne (read-line port))
         (chaîne (string-append chaîne ligne "\n")))
    (if (not (eof-object? (peek-char port)))
        (lire-tout-le-fichier port chaîne)
        chaîne)))

(define* (répondre #:optional corps #:key
                   (statut 200)
                   (paramètres-type-contenu '((charset . "utf-8")))
                   (type-contenu 'text/html)
                   (en-têtes-supplémentaires '()))
  (values (build-response
           #:code statut
           #:headers `((content-type
                        . (,type-contenu ,@paramètres-type-contenu))
                       ,@en-têtes-supplémentaires))
          corps))

(define (répondre-avec-fichier chemin)
  (let ((extension (cadr (string-split (basename chemin) #\.))))
    (cond ((string=? extension "css")
           (répondre (call-with-input-file chemin lire-tout-le-fichier)
                      #:type-contenu 'text/css))
          ((string=? extension "js")
           (répondre (call-with-input-file chemin lire-tout-le-fichier)
                     #:type-contenu 'application/javascript))
          (else (répondre (call-with-input-file chemin lire-tout-le-fichier))))))

(define (fichier-régulier? chemin)
  (let ((informations (false-if-exception (stat chemin))))
    (if (not informations)
        #f
        (eq? (stat:type informations) 'regular))))

(define (envoyer-fichier requête)
  (let* ((chemin (string-append racine (uri-decode (uri->string (request-uri requête)))))
	 (index (string-append racine "/" "index.html")))
    (if (and (string=? chemin (string-append racine "/"))
             (fichier-régulier? index))
        (répondre-avec-fichier index)
        (if (not (fichier-régulier? chemin))
            (non-trouvé requête)
            (répondre-avec-fichier chemin)))))

(define (gérer-texte requête corps)
  (if (eq? (request-method requête) 'POST)
      (let* ((texte-reçu (string-split (uri-decode (utf8->string corps)) #\=)))
        (if (string=? (car texte-reçu) "texte")
            (set! texte (cadr texte-reçu)))))
  (values '((content-type . (text/json)))
          (scm->json-string `(("texte" . ,texte)))))

(define (chaîne->liste-associative chaîne)
  (map (lambda (élément)
         (let ((liste (string-split élément #\=)))
           `(,(string->symbol (car liste)) . ,(cadr liste))))
       (string-split chaîne #\&)))

(define* (retourner-json objet #:key
                        (paramètres-type-contenu '((charset . "utf-8")))
                        (en-têtes-supplémentaires '()))
  (répondre (scm->json-string objet)
            #:paramètres-type-contenu paramètres-type-contenu
            #:type-contenu 'text/json
            #:en-têtes-supplémentaires en-têtes-supplémentaires))

(define (connexion requête corps)
  (cond ((eq? (request-method requête) 'GET) ;Si on contrôle la connexion
	 (let ((session (false-if-exception (string->number
					     (assq-ref (request-headers requête) 'cookie)))))
         (if (and session (session-correcte? session))
             (retourner-json '(("connecté" . #t)))
             (retourner-json '(("connecté" . #f))))))

        ((eq? (request-method requête) 'POST) ;Si on se connecte
         (let* ((identifiants (chaîne->liste-associative (uri-decode (utf8->string corps))))
                (utilisateur (assq-ref identifiants 'utilisateur))
                (mdp (assq-ref identifiants 'mdp)))
           (if (identifiants-corrects? utilisateur mdp)
               (let ((id-session (créer-identifiant-session)))
                 (if id-session
                     (begin
                       (set! sessions (append sessions `(,id-session)))
                       (retourner-json '(("connecté" . #t))
                                       #:en-têtes-supplémentaires
                                       `((set-cookie . ,(number->string id-session)))))
                     (retourner-json '(("connecté" . #f)))))
               (retourner-json '(("connecté" . #f))))))))

(define (déconnexion requête corps)
  (let ((session (string->number
                  (assq-ref (request-headers requête) 'cookie))))
    (if (session-correcte? session)
        (supprimer-session session)))
  (retourner-json '(("connecté" . #f))
                  #:en-têtes-supplémentaires
                  `((set-cookie . "Expires: Mon 01 Jan 2018 00:00:00 GMT"))))

(define (gestion-requêtes requête corps)
  (let ((composants (composants-chemin-requête requête)))
    (cond ((equal? composants '("texte"))
           (gérer-texte requête corps))
          ((equal? composants '("connexion"))
           (connexion requête corps))
          ((equal? composants '("déconnexion"))
           (déconnexion requête corps))
          (else (envoyer-fichier requête)))))

(run-server gestion-requêtes 'http '(#:port 8042))
