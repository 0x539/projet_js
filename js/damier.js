let Damier;
(function () {
    "use strict";

    Damier = function (largeur, longueur, destination) {
        this.largeur = largeur;
        this.longueur = longueur;
        this.destination = destination;
        this.tableau = [];
        this.joueur_un = true;
        let self = this;

        let créer_case = function (y, x) {
            return $('<td />').data('x', x).data ('y', y).html('&nbsp;&nbsp;&nbsp;').click (function () {
                $(this).unbind("click");
                let x = $(this).data('x');
                let y = $(this).data('y');
                $(this).html (self.joueur_un ? "X" : "O");
                self.tableau[y][x] = self.joueur_un;
                if (ligne_faite (self.joueur_un)) {
                    if (self.joueur_un)
                        $('body').append ($('<p />').html('Le premier joueur a gagné.'));
                    else
                        $('body').append ($('<p />').html('Le second joueur a gagné.'));
                } else {
                    self.joueur_un = !self.joueur_un;
                }
            });
        };

        let créer_case_blanche = function (y, x) {
            return créer_case (y, x).addClass('case-blanche');
        };

        let créer_case_noire = function (y, x) {
            return créer_case (y, x).addClass('case-noire');
        };

        let vérifier_colonnes = function (joueur) {
            let trouvé = false;

            for (let i = 0; i < self.tableau.length; ++i) {
                trouvé = true;
                for (let j = 0; j < self.tableau[i].length; ++j) {
                    if (self.tableau[j][i] != joueur) {
                        trouvé = false
                        break;
                    }
                };

                if (trouvé) {
                    return true;
                }
            };

            return false;
        };

        let vérifier_lignes = function (joueur) {
            let trouvé = false;

            for (let i = 0; i < self.tableau.length; ++i) {
                trouvé = true;
                for (let j = 0; j < self.tableau[i].length; ++j) {
                    if (self.tableau[i][j] != joueur) {
                        trouvé = false
                        break;
                    }
                };

                if (trouvé) {
                    return true;
                };
            };

            return false;
        };

        let vérifier_diagonale_droite = function (joueur) {
            if (self.tableau[2][0] == joueur && self.tableau[1][1] == joueur && self.tableau[0][2] == joueur) {
                return true;
            }

            return false;
        };

        let vérifier_diagonale_gauche = function (joueur) {
            if (self.tableau[0][0] == joueur && self.tableau[1][1] == joueur && self.tableau[2][2] == joueur) {
                return true;
            }

            return false;
        };

        let ligne_faite = function (joueur) {
            if (vérifier_colonnes (joueur)) {
                return true;
            }
            else if (vérifier_lignes (joueur)) {
                return true;
            }
            else if (vérifier_diagonale_droite (joueur)) {
                return true;
            }
            else if (vérifier_diagonale_gauche (joueur)) {
                return true;
            }
            else {
                return false;
            }
        };

        let damier = $(this.destination);
        for (let i = 0; i < this.longueur; i++) {
            let tr = $('<tr />');
            let tab = [];
            for (let j = 0; j < this.largeur; j++) {
                tab[j] = null;
                if (i % 2 == 0) {
                    if (j % 2 == 0) {
                        let td = créer_case_noire (i, j);
                        tr.append (td);
                    }
                    else {
                        let td = créer_case_blanche (i, j);
                        tr.append (td);
                    }
                }
                else {
                    if (j % 2 == 0) {
                        let td = créer_case_blanche (i, j);
                        tr.append (td);
                    }
                    else {
                        let td = créer_case_noire (i, j);
                        tr.append (td);
                    }
                }
            }
            this.tableau[i] = tab;
            damier.append (tr);
        }
    }
} ());
