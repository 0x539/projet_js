(function () {
    "use strict";

    let erreur_critique = function() {
        $('body').html('Une erreur inattendue est survenue.<br>');
    };

    let intervalle_màj_texte = false;
    let texte = "";

    $(() => {
        let obtenir_changements = () => {
            $.ajax ({
                url: 'texte',
                method: 'GET'})
                .done ( (données) => {
                    texte = données.texte;
                });
        };

        let màj_texte = () => {
            $('.texte').val (texte);
        };

        $.ajax ({
            url: '/connexion'
        }) .done (function (données) {
                if (données.connecté) {
                    $('#déconnexion').show ();
                    $('body').append($('<h1/>').html('Éditeur de texte collaboratif'));
                    $('body').append('<textarea autofocus class="texte"></textarea>');

                    setInterval(obtenir_changements, 500);
                    intervalle_màj_texte = setInterval(màj_texte, 200);

                    $(".texte").on ('input', (évènement) => {
                        clearInterval(intervalle_màj_texte);

                        setTimeout ( () => {
                            $.post ('texte', { 'texte': $('.texte').val () })
                                .fail (erreur_critique);
                        }, 200);

                        setTimeout ( () => {
                            clearInterval(intervalle_màj_texte);
                            intervalle_màj_texte = setInterval(màj_texte, 500);
                        }, 1000);
                    });
                } else {
                    $('#connexion').show ();
                }
            }).fail(erreur_critique)

        $('#connexion').submit(function() {
            $.ajax({
                url: $(this).attr('action'),
                method: $(this).attr('method'),
                data: $(this).serialize()
            }) .done(function (données) {
                if (données.connecté) {
                    window.location.reload(true);
                }
                else {
                    $('#mdp').val ('');
                }
            }) .fail(erreur_critique);

            return false;
        });

        $('#déconnexion').submit(function() {
            $.ajax({
                url: $(this).attr('action'),
                method: $(this).attr('method')})
                .done(() => {window.location.reload (true);})
                .fail(erreur_critique);
            return false;
        });
    });
}) ();
